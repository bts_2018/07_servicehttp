import { Injectable } from '@angular/core';
import { Hero } from './../classes/hero';
import { Http, Headers, Response, RequestOptions} from '@angular/http'
import 'rxjs/add/operator/toPromise';

@Injectable()
export class HeroService {

  heroes:Hero[];

  constructor (private http:Http){
    
  }

  getHeroes(): Promise<Hero[]> {     
    return this.http.get("http://www.winelove.club/heroes/list.php?token=5a7875d7e843b")
        .toPromise()
        .then (this.extractData)
        .catch (this.handleError);
  }
  
  private handleError(error: Response): Promise<any> {
    return Promise.reject(error);  
  }

  private extractData(res: Response): Promise<Hero[]> {
    return res.json();
  }
 

}
