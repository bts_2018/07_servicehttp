import { Component } from '@angular/core';
import { Hero } from '../classes/hero';
import { HeroService } from '../services/hero.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent  {
  
  heroes:Hero[];
  selectedHero:Hero;

 constructor(private heroService: HeroService) {   
       this.heroService.getHeroes()
        .then(heroes => this.heroes = heroes)
        .catch(error => console.log(error));
  }

  onSelect(hero: Hero): void {
    this.selectedHero = hero;
     console.log(this.selectedHero);
  }
 
}
